# Quasar App (website)

A Quasar Project

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

# TODO

- [ ] Getting started
- [ ] The RoboRio
- [ ] The Radio
- [ ] Networking
- [ ] The Driver Station
- [ ] Labview
- [ ] Java
- [ ] Getting Git
- [ ] CI/CD
- [ ] Vision
- [ ] Sensing Color
- [ ] Autonomous Automation
- [ ] PID
- [ ] Encoding Encoders
- [ ] Limiting Switches
- [ ] CTRE Software
- [ ] Rev Robotics Software
- [ ] The NavX
