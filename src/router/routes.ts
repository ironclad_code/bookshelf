import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/IndexPage.vue') }],
  },

  {
    path: '/sus',
    component: () => import('layouts/emptyLayout.vue'),
    children: [{ path: '', component: () => import('pages/sus.vue') }],
  },

  {
    path: '/docs',
    component: () => import('layouts/DocsLayout.vue'),
    children: [{ path: '', component: () => import('pages/docs.vue') }],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
